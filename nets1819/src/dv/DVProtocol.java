package dv;

import peersim.cdsim.CDProtocol;
import peersim.config.FastConfig;
import peersim.core.Linkable;
import peersim.core.Network;
import peersim.core.Node;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * The class implements a network layer Distance-Vector protocol.
 * It is based on the original Link-State protocol class.
 *
 * The protocol broadcast its local graph to all nodes in the network.
 * Each instance of the protocol then computes the shortest path tree using the Bellman-Ford algorithm.
 *
 * @author A. Kostarevas, M. Ayiad
 * @version 1.0
 * April 2018
 */
public class DVProtocol implements CDProtocol {
    private enum State {
        INITIALISE, BROADCAST, COMPUTE
    }
    private ArrayList<Edge> graph;
    private TreeMap<Long, Path> paths;
    private State phase;
    public boolean fin = false;
    public int CC = 0;	//Cycle count

    /**
     * A constructor.
     *
     * @param prefix required by PeerSim to access protocol's alias in the configuration file.
     */
    public DVProtocol(@SuppressWarnings("unused") String prefix) {
        /* Start in INIT phase */
        this.phase = State.INITIALISE;
        /* Enable computation cycle */
        //this.fin = false;
    }

    /**
     * PeerSim cyclic service. To be execute every cycle.
     *
     * @param host Reference to host node.
     * @param pid Global protocol's ID in this simulation.
     */
    @Override
    public void nextCycle(Node host, int pid) {
        /* Reference local Linkable protocol */
        Linkable lnk = (Linkable) host.getProtocol(FastConfig.getLinkable(pid));
        /* Reference host's node ID */
        long nodeId = host.getID();
        /* Current phase */
       switch (phase) {
            case INITIALISE:
                init(lnk, nodeId);
                phase = State.BROADCAST;
                break;
            case BROADCAST:
                broadcast(pid);
                phase = State.COMPUTE;
                break;
            case COMPUTE:
                compute(nodeId);
                break;
        }
        CC++;
    }

    /**
     * Initialises local graph.
     *
     * @param lnk Reference local Linkable protocol.
     * @param nodeId Host Node ID.
     */
    private void init(Linkable lnk, long nodeId) {
        long neighborId;
        this.graph = new ArrayList<>();	//Intialising the data holders
        this.paths = new TreeMap<>();
        for (int i = 0; i < lnk.degree(); i++) {	//for every node.
            neighborId = lnk.getNeighbor(i).getID();	//find neighbour node.
            graph.add(new Edge(nodeId, neighborId, 1));	//add the edges to the graph
            									//Set the cost to 1, for all of them, as per the spec.
        }
    }

    /**
     * Broadcasts local graph to peers.
     * @param pid Global protocol's ID in this simulation.
     */
    private void broadcast(int pid) {
        /* Get network size */
        int size = Network.size();
        /* Broadcast to all nodes */
        for (int i = 0; i < size; i++) {
            /* Access node i */
            Node tempNode = Network.get(i);
            /* Access DV protocol in node i */
            DVProtocol tempProtocol = (DVProtocol) tempNode.getProtocol(pid);
            /* Copy local graph */
            ArrayList<Edge> tempGraph = new ArrayList<>(graph);
            /* Send the copy to node i */
            tempProtocol.receive(tempGraph);
        }
    }

    /**
     * Compute shortest path using Bellman-Ford algorithm.
     * @param nodeId Host Node ID.
     */
    private void compute(long nodeId) {
        /* Do it only once */
        if (fin) {
            return;
        }
        /* Initialise graph */
        for (long i = 0; i < Network.size(); i++) {
            paths.put(i, new Path(i, i, Integer.MAX_VALUE));
        }
        /* Source node costs 0 */
        paths.get(nodeId).cost = 0;
        for (int i = 0; i < (Network.size() - 1); i++) {	//For every node
            for (Edge edge : graph) {	//for every graph
                long Edgesrc = edge.source;	//assigning edge source ID
                long Edgedst = edge.destination;	//assigning edge destination ID
                if (paths.get(Edgesrc).cost >= Integer.MAX_VALUE) {// If the path is infinite. i.e, the nodes aren't linked.
                    continue;
                }
                if (paths.get(Edgesrc).cost + edge.cost < paths.get(Edgedst).cost) {	//relaxing section
                    paths.get(Edgedst).cost = paths.get(Edgesrc).cost + edge.cost;
                    if (edge.source == nodeId) {	//if the edge source is the host
                        paths.get(Edgedst).predecessor = edge.destination;	
                    } else {
                        paths.get(Edgedst).predecessor = edge.source;
                    }
                }
            }
        }
        /* Check for negative-weight cycles */
        for (Edge edge : graph) {
            if (paths.get(edge.source).cost + edge.cost < paths.get(edge.destination).cost) {
                System.out.println("Graph contains a negative-weight cycle");
            }
        }
        fin = true;	//finished the protocol
    }

    /**
     * Receives a graph form a neighbour and updates local graph
     * removes duplicate edges if any
     *
     * @param neighborGraph a copy of neighbour's local graph
     */
    private void receive(ArrayList<Edge> neighborGraph) {
        /* For each edge in the neighbour's graph */
        for (Edge aNeighborGraph : neighborGraph) {
            /* Ignore duplicate edges */
            boolean duplicate = false;
            for (Edge aGraph : graph) {
                if (aNeighborGraph.equals(aGraph)) {
                    duplicate = true;
                    break;
                }
            }
            /* Add new edge */
            if (!duplicate) {
                graph.add(aNeighborGraph);
            }
        }
    }

    /**
     * Access to local path tree. Used by the observer.
     *
     * @return the local path tree
     */
    public TreeMap<Long, Path> getPaths() {
        return paths;
    }

    /**
     * used by PeerSim to clone this protocol at the start of the simulation
     */
    @Override
    public Object clone() {
        Object o = null;
        try {
            o = super.clone();
        } catch (Exception ignored) { }
        return o;
    }
}

package dv;

import java.util.TreeMap;
import peersim.config.Configuration;
import peersim.core.Control;
import peersim.core.Network;
import peersim.core.Node;

/**
 * The class observes DistanceVectorProtocol protocol and provide readable printout.
 * The observer prints to output screen the shortest path tree form each source node and in each cycle.
 * PREREQUISITE, must be used for DistanceVectorProtocol protocol in PeerSim.
 * @author M. Ayiad
 * @version 1.0 
 * March 2018
 */
public class DVObserver implements Control{

	private final int pid;											//DistanceVectorProtocol protocol ID
	/**
	 * A constructor
	 * @param prefix a string provided by PeerSim and used to access parameters from the configuration file.
	 */
	public DVObserver(String prefix) {
		this.pid  = Configuration.getPid(prefix + ".protocol");
	}
	
	/**
	 * Implementation of the common method. This method is called in each cycle. 
	 */
	@Override
	public boolean execute() {
		for(int i=0; i < Network.size(); i++ ) {										//for each node in network
			
			Node node = Network.get(i);													//reference node i
			DVProtocol  protocol = (DVProtocol) node.getProtocol(pid);	//get DV protocol
			TreeMap<Long, Path> paths = protocol.getPaths();							//get path tree
						
			if(paths==null || protocol.CC < 3) continue;	//if paths aren't loaded, or it's still initialising or broadcasting									
																			
			System.out.printf("ND#%3d->| ", Network.get(i).getID());	//Print the host Node.
			for(Path p : paths.values()) System.out.printf("%3d,", p.destination );	//Print the destination nodes.
			System.out.printf(" |%s ", "");
			if (protocol.fin == true) System.out.print("STOPPED\n");	//if the protocol has stopped updating, show this.
			else System.out.print("UPDATE\n");
			System.out.print("    hops| "); 	//print the number of hops from 1 node to another
			for(Path p : paths.values()) 
				if(p.cost == Integer.MAX_VALUE)	
					System.out.printf("  X," );	//if not possible to get to that node.
				else
					System.out.printf("%3d,", p.cost );	
			System.out.printf(" |\n     via| ");
			for(Path p : paths.values()) System.out.printf("%3d,", p.predecessor ); //print out the node it has to go through to get to the next destination one.
			System.out.printf(" |\n----------------------------------------------------------------------------\n");
		}
		return false;
	}
}

package simple;

import peersim.cdsim.CDProtocol;
import peersim.config.FastConfig;
import peersim.core.*;
import peersim.vector.SingleValueHolder;

public class SimpleProtocol extends SingleValueHolder implements CDProtocol {
	// define server node ID
	public static int SERVER_NODE_ID = 0;
	// a contractor as required by 𝑃𝑒𝑒𝑟𝑆𝑖𝑚
	public double avgValue = 0.0;
	public int serNum = 0;
	public SimpleProtocol(String arg0) {
		super(arg0);
	}

	// implements CDProtocol methods, this will be called every cycle
	public void nextCycle(Node host, int protocolID) {
		int linkableID = FastConfig.getLinkable(protocolID);
		Linkable linkable = (Linkable)host.getProtocol(linkableID);
		// is this node a server
		if (host.getID() == SERVER_NODE_ID) {
			System.out.printf("Server value = %f \n", value);
			System.out.printf("Average: %f ", findAvg());
			value = 0;
		} else { // No it is a host node
			// get the server node
			Node server = Network.get(SERVER_NODE_ID);
			// get protocol
			SimpleProtocol protocol = (SimpleProtocol) server.getProtocol(protocolID);
			// use the protocol
			protocol.addValue(this.value);
			protocol.addValue4Avg(this.value);
		}
	}

	private void addValue4Avg(double value) {
		avgValue += value;
		serNum +=1;
	}

	// a public add service of the protocol
	public void addValue(double value) {
		this.value += value;
	}
	
	public double findAvg() {
		return avgValue/serNum;
	}
}
